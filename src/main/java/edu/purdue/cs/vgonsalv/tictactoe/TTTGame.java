/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.tictactoe;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author Vic
 */
public class TTTGame {

    private final PropertyChangeSupport pcs;
    private final List<TTTMoveSource> moveSources;
    private final TTTBoard board;
    private int turn = 0;
    private boolean playing;

    public TTTGame() {
        this.pcs = new PropertyChangeSupport(this);
        this.moveSources = Arrays.asList(null,null);
        this.board = new TTTBoard();
        board.addPropertyChangeListener("board",pcs::firePropertyChange);
        this.playing=false;
    }

    public boolean canMove() {
        return turn < 9;
    }

    private boolean isValidMove(int move, Player player) {
        if(moveSources.get(getCurrentTurn()).getPlayer().equals(player)){
            return board.getBoard(move).equals(Mark.EMPTY);
        }
        return false;
    }
    private boolean allMatch(int []locations){
        List<Mark> marks = Arrays.stream(locations).mapToObj(board::getBoard).collect(Collectors.toList());
        return !marks.get(0).equals(Mark.EMPTY) && marks.get(0)==marks.get(1)&& marks.get(1)==marks.get(2);
    }
    public Optional<Player> getWinner(){
        int[][] locations = {{0,1,2},{3,4,5},{6,7,8},{0,4,8},{2,4,6}};
        Optional<int[]> location = Arrays.stream(locations).filter(this::allMatch).findAny();
        return location.map(arr -> board.getBoard(arr[0]).getPlayer());
    }
    public void setMoveSource(int player, TTTMoveSource source){
        pcs.fireIndexedPropertyChange("moveSource", player, moveSources.get(player), source);
        moveSources.set(player, source);
    }

    public int getCurrentTurn() {
        return turn%2;
    }
    public int getTurn(TTTMoveSource src){
        return moveSources.indexOf(src);
    }
    public Player getOpponent(TTTMoveSource src){
        return moveSources.get(1 - getTurn(src)).getPlayer();
    }

    public void setCurrentTurn(int turn) {
        pcs.firePropertyChange("turn", getCurrentTurn(), turn);
        this.turn = turn;
    }

    public boolean isPlaying() {
        return playing;
    }

    private void setPlaying(boolean isPlaying) {
        pcs.firePropertyChange("playing", isPlaying(), isPlaying);
        this.playing = isPlaying;
    }
    

    public void play() {
        board.clear();
        setPlaying(true);
        while (canMove()& !getWinner().isPresent()) {
            TTTMoveSource src;
            int move;
            do {
                src = moveSources.get(getCurrentTurn());
                move = src.getMove();
            } while (!isValidMove(move, src.getPlayer()));
            board.setBoard(move, src.getPlayer().getMark());
            setCurrentTurn((getCurrentTurn()+1));
        }
        setPlaying(false);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    public TTTBoard getBoard() {
        return board;
    }

    TTTMoveSource getMoveSource(int i) {
        return moveSources.get(i);
    }

    
}
