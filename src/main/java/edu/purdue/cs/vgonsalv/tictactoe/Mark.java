/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.tictactoe;

import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 *
 * @author Vic
 */
public class Mark implements Serializable{
    public static final Mark EMPTY = new Mark(){

        @Override
        public void setColor(Color color) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setSymbol(String symbol) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setPlayer(Player player) {
            throw new UnsupportedOperationException();
        }
        
    };
    private Player player;
    private String symbol;
    private Color color;
    
    private final PropertyChangeSupport pcs;

    public Mark() {
        this.pcs = new PropertyChangeSupport(this);
    }

    public Mark(Player player, String symbol, Color color) {
        this();
        this.player = player;
        this.symbol = symbol;
        this.color = color;
    }
    
    public Mark(Mark mark){
        this(mark.getPlayer(),mark.getSymbol(),mark.getColor());
    }

    
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        pcs.firePropertyChange("player", getPlayer(), player);
        this.player = player;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        pcs.firePropertyChange("symbol", getSymbol(), symbol);
        this.symbol = symbol;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        pcs.firePropertyChange("color", getColor(), color);
        this.color = color;
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }
}
