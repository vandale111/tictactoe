/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.tictactoe;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Vic
 */
public class TTTBoard implements Serializable{
    private final List<Mark> board;
    private final PropertyChangeSupport  pcs;
    
    public TTTBoard(){
        Mark[] tmpBoard = new Mark[9];
        Arrays.fill(tmpBoard, Mark.EMPTY);
        this.board = Arrays.asList(tmpBoard);
        this.pcs = new PropertyChangeSupport(this);
    }
    public void clear(){
        for(int i=0;i<9;i++)
            setBoard(i,Mark.EMPTY);
    }
    public void setBoard(int position, Mark mark){
        Object old = getBoard(position);
        board.set(position, mark);
        pcs.fireIndexedPropertyChange("board", position,old , mark);
    }
    public Mark getBoard(int position){
        return board.get(position);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }
    
}
