/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.tictactoe;

import java.beans.IndexedPropertyChangeEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vic
 */
public class RemoteClient implements TTTMoveSource,AutoCloseable{
    private final ObjectInputStream in;
    private final ObjectOutputStream  out;
    private Player player;

    public RemoteClient(Socket s) throws IOException {
        this.in = new ObjectInputStream(s.getInputStream());
        this.out = new ObjectOutputStream(s.getOutputStream());
    }
    
    @Override
    public int getMove() {
        try{
            return (Integer)in.readObject();
        }catch(IOException ex){
            throw new RuntimeException("IOException has occured", ex);
        }catch(ClassNotFoundException ex){
            throw new RuntimeException("ClassNotFoundException has occured", ex);
        }catch(ClassCastException ex){
            throw new IllegalStateException("Expected to read int", ex);
        }
    }

    @Override
    public void registerGame(TTTGame game) {
        game.addPropertyChangeListener("board", event->{
            try {
                IndexedPropertyChangeEvent ievent = (IndexedPropertyChangeEvent) event;
                Mark played = (Mark) ievent.getNewValue();
                if(played.getPlayer()!=player){
                    out.writeObject(ievent.getIndex());
                    out.flush();
                }
            } catch (IOException ex) {
                Logger.getLogger(RemoteClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        try {
            double mine,other;
            do{
                out.writeDouble(mine=Math.random());
                out.flush();
                other = in.readDouble();
            }while(mine == other);
            int turn = mine>other?0:1;
            game.setMoveSource(turn, this);
            out.writeObject(game.getOpponent(this));
            out.flush();
            player = (Player) in.readObject();
            
        } catch (IOException| ClassNotFoundException ex) {
            Logger.getLogger(RemoteClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public Player getPlayer() {
        return this.player;
    }

    @Override
    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public void close() {
        try {
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(RemoteClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(RemoteClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
