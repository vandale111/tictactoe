/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.tictactoe;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 *
 * @author Vic
 */
public class Player implements Serializable{
    private String name;
    private final Mark mark;
    private final PropertyChangeSupport pcs;

    public Player() {
        this(null,new Mark());
    }

    public Player(String name, Mark mark) {
        this.name = name;
        this.mark = mark;
        this.pcs = new PropertyChangeSupport(this);
        this.mark.setPlayer(this);
    }
    
    public Player(Player player){
        this(player.getName(),new Mark(player.getMark()));
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        pcs.firePropertyChange("name", getName(), name);
        this.name = name;
    }

    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark mark) {
        pcs.firePropertyChange("mark", getMark(), name);
        this.mark.setColor(mark.getColor());
        this.mark.setSymbol(name);
    }
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }
    
}
