/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.tictactoe;

import java.awt.Color;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Vic
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter name:");
        String name = input.nextLine();
        System.out.println("Enter symbol to use:");
        String symbol = input.nextLine().substring(0,1);
        
        Player player = new Player(name, new Mark(null, symbol, Color.yellow));
        System.out.println("Wait for connection?[y/n]");
        String line = input.nextLine();
        Socket remote;
        if (line.charAt(0) == 'y' || line.charAt(0) == 'Y') {
            System.out.println("Enter local port to listen on:");
            int port = input.nextInt();
            ServerSocket ss = new ServerSocket(port);
            remote = ss.accept();
        }else{
            System.out.println("Enter host address");
            String host = input.nextLine();
            System.out.println("Enter port");
            int port = input.nextInt();
            remote = new Socket(host, port);
        }
        remote.setTcpNoDelay(true);
        RemoteClient remoteClient = new RemoteClient(remote);
        TerminalClient localClient = new TerminalClient();
        localClient.setPlayer(player);
        
        TTTGame game = new TTTGame();
        game.setMoveSource(0, localClient);
        game.setMoveSource(1, localClient);
        
        localClient.registerGame(game);
        remoteClient.registerGame(game);
        
        game.play();
    }

}
