/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.tictactoe;

import java.awt.Color;
import java.io.PrintStream;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author Vic
 */
public class TerminalClient implements TTTMoveSource {

    private Player player;
    private final PrintStream out;
    private final Scanner in;
    private TTTGame game;

    public TerminalClient() {
        this.in = new Scanner(System.in);
        this.out = System.out;
    }
    

    @Override
    public int getMove() {
        int move = -1;
        do {
            out.printf("Enter move:\n");
            String line = in.nextLine().replaceAll("\\W", "");
            if(line.length()!=2){
                out.printf("Invalid input\n");
                continue;
            }
            int row,col;
            if(Character.isDigit(line.charAt(0))){
                col  = line.charAt(0)-'1';
                row  = Character.toLowerCase(line.charAt(1))-'a';
            }else{
                col  = line.charAt(1)-'1';
                row  = Character.toLowerCase(line.charAt(0))-'a';
            }
            move = row* 3 + col;
            if(move>9 || move <0){
                out.printf("invlid move\n");
                continue;
            }
        } while (move < 0 || move > 8);
        return move;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public void setPlayer(Player player) {
        this.player = player;
    }

    private String drawBoard(TTTBoard board) {
        return IntStream.range(0, 3)
                .mapToObj(i -> IntStream.range(i * 3, i * 3 + 3)
                        .mapToObj(board::getBoard)
                        .map(m -> m.equals(Mark.EMPTY) ? " " : m.getSymbol())
                        .collect(Collectors.joining(" | ", ((char)('a' + i)) + " | ", "")))
                .collect(Collectors.joining("\n--------------\n", "    1   2   3\n", ""));
    }

    @Override
    public void registerGame(TTTGame game) {
        this.game = game;
        game.addPropertyChangeListener("board", event -> {
            out.printf("%s\n", drawBoard(game.getBoard()));
        });
        game.addPropertyChangeListener("playing", event -> {
            if ((boolean) event.getNewValue()) {
                out.printf("game begining\n");
                out.printf("Playing as %s against %s(%s)\n",
                        player.getMark().getSymbol(),
                        game.getOpponent(this).getName(),
                        game.getOpponent(this).getMark().getSymbol());
                out.printf("%s\n", drawBoard(game.getBoard()));
            } else {
                out.printf("game over\n");
                Optional<Player> winner = this.game.getWinner();
                if (winner.isPresent()) {
                    out.printf("%s won\n", winner.get().getName());
                } else {
                    out.printf("Draw\n");
                }
            }
        });
    }
    
}
